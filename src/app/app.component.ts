import { Component } from '@angular/core';

interface Fruit {
  name: string;
  color: string;  
}

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent  {
  name = 'Angular';

  fruits: Fruit[] = [{
    name: 'Banana',
    color: 'yellow'
  }, {
    name: 'Orange',
    color: 'orange'
  }, {
    name: 'Apple',
    color: 'red'
  }, {
    name: 'Grape',
    color: 'violet'
  }];

  up(index, item) {
    if (index > 0) {                  
      const deleted = this.fruits.splice(index - 1, 1, this.fruits[index]);
      this.fruits.splice(index, 1, deleted[0]);
    }
  }

  down(index, item) {    
    if (index < this.fruits.length - 1) {                  
      const deleted = this.fruits.splice(index + 1, 1, this.fruits[index]);
      this.fruits.splice(index, 1, deleted[0]);
    }
  }

}
